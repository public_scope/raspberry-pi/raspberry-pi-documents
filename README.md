# [![i1][logo-full-sm]][raspberry-url]

| [Interactive Kiosks][kiosk-md] | [Raspberry Pi Downloads][raspberry-download-url] |
|:-:|:-:|
| [![i2][kiosk]][kiosk-md] | [![i3][logo-icon-sm]][raspberry-download-url] |

[logo-full-lg]: /docs/images/rasp-logo.png "Raspberry Pi 3"
[logo-full-sm]: /docs/images/rasp-logo-sm.png "Raspberry Pi 3"
[logo-icon-sm]: /docs/images/RPi-Logo-Reg-SCREEN.png
[kiosk]: /docs/images/kiosk-icon-5.png "Raspberry Interactive Kiosks"

[raspberry-url]: https://www.raspberrypi.org/ "Raspberry Pi"
[raspberry-download-url]: https://www.raspberrypi.org/downloads/ "Raspberry Pi Downloads"
[kiosk-md]: /docs/kiosk.md "Instructions on creating a Rasp-Kiosk from scratch"