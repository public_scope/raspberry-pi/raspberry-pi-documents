# [![i1][logo-full-sm]][raspberry-url]

###### **NOTE:** *This page is not interactive, although there are checkboxes beside each task, you are not able to place a check in them as you have completed each step. To utilitize the checkboxes, print this page.*

## Gather the items that will be needed

- [ ] Get a Pi 3 and power supply
- [ ] Get a SD card minimum 8 GB - maximum 32 GB
- [ ] Get a monitor and HDMI cable
- [ ] Get a network cable
- [ ] If needed, get a micro SD or SD card reader/writer for your computer

## Install the hardware

- [ ] Plug in the HDMI and power cable to the monitor*
- [ ] Plug the HDMI into the Pi
- [ ] Plug the SD Card reader into your computer

## Start the downloads

- [ ] Download [Raspbian Stretch][raspbian]
- [ ] Download [Etcher][etcher]
- [ ] Download [Bitvise SSH][bitvise]

## Start the installs

- [ ] Install Etcher
- [ ] Extract Raspbian to a folder
- [ ] Load Etcher
- [ ] Select the extracted ISO from the downloaded Raspbian
- [ ] Select the SD Card to Flash to
- [ ] Click on flash
- [ ] Install Bitvise SSH Client

## Setup the Pi

- [ ] Make sure the Pi is unplugged from power
- [ ] Install the micro SD card into the bottom of the pi
- [ ] Plug the power into the pi

## Start the configuration

- [ ] Default user name `pi` and default password `raspberry`
- [ ] Type in `ifconfig` and look for the ether mac address
- [ ] Type in `passwd`
- [ ] Enter the current password of: raspberry
- [ ] Type in a new password  
<sub>**NOTE:** *Password will not change until you reboot, but we are still waiting on the DHCP, so continue using raspberry as the password until you reboot.*</sub>
- [ ] Type in `sudo raspi-config`
- [ ] Select Boot Options
- [ ] Select Desktop / CLI
- [ ] Select Console Autologin
- [ ] Select Network Options
- [ ] Select N1 Hostname
- [ ] Select Localisation Options
- [ ] Select Change Locale
- [ ] Scroll all the way to en_GB and use space bar to unselect it
- [ ] Scroll to en_US ISO 8859-1 and use space bar to select it
- [ ] Press enter
- [ ] choose en_US
- [ ] Go to Localisation again
- [ ] Select Change Timezone
- [ ] Select America
- [ ] Select New York
- [ ] Select Interfacing
- [ ] Select SSH and Yes to enable
- [ ] Select Advanced Options
- [ ] Select Expand Filesystem
- [ ] Select Advanced Options
- [ ] Select Resolution
- [ ] Select DMT Mode 82 (1920x1080 60Hz 16:9)
- [ ] Select Finish
- [ ] Select Yes to reboot now
- [ ] Once rebooted, type in `ifconfig` to get the ether IP address

## Connect to Pi & Work comfortably from your desktop :)

- [ ] Load Bitvise SSH Client
- [ ] In the Host type the IP address for the pi
- [ ] User Name: `pi` Password `<YOUR PASSWORD>`
- [ ] Click on Login
- [ ] If the Certificate warning shows, click on Accept and Save
- [ ] A black window will show up and your connected to the pi

## Setup the Pi

###### **NOTE:** *If using a TV for the monitor, then run: `/opt/vc/bin/tvservice -m CEA`. This will check the best mode. The TV screen has activated CEC support (called simplink). I use this feature to automatically turn on the TV while Pi is booting.*

- [ ] Type in `sudo nano /boot/config.txt`
- [ ] Remove the `#` from `disable_overscan=1`
- [ ] Remove the `#` from `framebuffer_width=`
- [ ] Remove the `#` from `framebuffer_height=`
- [ ] Change the numbers to the resolution for your monitor or TV.
- [ ] `Ctrl-O <ENTER>` to save the file and `Ctrl-X` to exit
- [ ] Type in `ifconfig` to see if you have a 10. or 192. IP address, if so, then type `sudo apt update && sudo apt upgrade -y`
- [ ] Type in `sudo apt dist-upgrade`
- [ ] Type in `sudo apt install -y matchbox chromium-browser cec-utils xinit x11-xserver-utils ttf-mscorefonts-installer xwit sqlite3 libnss3`  
<sub>**NOTE:** *This will take a REALLY long time*</sub>
- [ ] Type in `sudo systemctl reboot`
- [ ] Bitvise will disconnect and once the pi is booted, it will reconnect, you will need to open a new Terminal Window (You can close the old one)
- [ ] Type in `sudo nano /etc/rc.local`
- [ ] Above the exit 0 line, add:

```sh
  if [ -f /boot/xinitrc ]; then
    chmod 0660 /dev/tty*
    ln -fs /boot/xinitrc /home/pi/.xinitrc;
    su - pi -c 'startx' &
  fi
```

- [ ] Save and Close `Ctrl-O <enter> Ctrl-X`
- [ ] Type in `sudo nano /etc/X11/Xwrapper.config` change `allowed_users=` to `anybody`
- [ ] Type in `sudo nano /boot/xinitrc`  
<sub>**NOTE:** *This is a new file, so the page will be blank, if not, then make a copy of the old one first.*</sub>
- [ ] Paste this script and change the URL to the location you want to point to

```sh
#!/bin/sh

while true; do
    # Turn on TV
    echo "on 0" | cec-client -s

    # Clean up previously running apps, gracefully at first then harshly
    killall -TERM chromium-browser 2>/dev/null;
    killall -TERM matchbox-window-manager 2>/dev/null;
    sleep 2;
    killall -9 chromium-browser 2>/dev/null;
    killall -9 matchbox-window-manager 2>/dev/null;

    # Clean out existing profile information
    rm -rf /home/pi/.cache;
    rm -rf /home/pi/.config;
    rm -rf /home/pi/.pki;

    # Generate the bare minimum to keep Chromium happy!
    mkdir -p /home/pi/.config/chromium/Default
    sqlite3 /home/pi/.config/chromium/Default/Web\ Data "CREATE TABLE meta(key LONGVARCHAR NOT NULL UNIQUE PRIMARY KEY, value LONGVARCHAR); INSERT INTO meta VALUES('version','46'); CREATE TABLE keywords (foo INTEGER);";

    # Disable DPMS / Screen blanking
    xset -dpms
    xset s off

    # Reset the framebuffer's colour-depth
    fbset -depth $( cat /sys/module/*fb*/parameters/fbdepth );

    # Hide the cursor (move it to the bottom-right, comment out if you want mouse interaction)
    xwit -root -warp $( cat /sys/module/*fb*/parameters/fbwidth ) $( cat /sys/module/*fb*/parameters/fbheight )

    # Start the window manager (remove "-use_cursor no" if you actually want mouse interaction)
    matchbox-window-manager -use_titlebar no -use_cursor no &

    chromium-browser --disable-translate --app=https://orion-svr.meyertool.com/Orion.Horizon/Metrics/WeeklyProgress?planId=17762
done;
```

[logo-full-sm]: /docs/images/rasp-logo-sm.png "Raspberry Pi 3"
[monitor-ports]: /docs/images/ports-monitor.jpg "Ports on the monitor or TV"
[raspberry-pi-hdmi]: /docs/images/raspberry_pi_model_b_plus.jpg "HDMI Port on Raspberry Pi"

[raspberry-url]: https://www.raspberrypi.org/ "Raspberry Pi"
[raspbian]: https://www.raspberrypi.org/downloads/raspbian/ "Raspbian Stretch Download"
[etcher]: https://www.balena.io/etcher/ "Flash OS images to SD cards & USB drives, safely and easily."
[bitvise]: https://www.bitvise.com/ssh-client-download "Free and flexible SSH Client for Windows"